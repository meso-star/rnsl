/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* setenv */

#include "rnsl.h"
#include <rsys/mem_allocator.h>

#include <stdlib.h>
#include <string.h>

static void
test_load(struct rnsl* rnsl)
{
  const char* path = "file.txt";
  FILE* fp = NULL;

  CHK(fp = fopen(path, "w+"));
  fprintf(fp, "8\n");
  fprintf(fp, "string0\n");
  fprintf(fp, "my/string/1\n");
  fprintf(fp, "my string\t2\n");
  fprintf(fp, "\"my string\t3\"\n");
  fprintf(fp, "\"${VARIABLE}/hello\"\n");
  fprintf(fp, "world!\n");
  fprintf(fp, "\n");
  fprintf(fp, "# Comment\n");
  fprintf(fp, "  foo\n");
  fprintf(fp, "\tbar\n");
  fprintf(fp, "unreachable string\n");
  rewind(fp);

  CHK(rnsl_load(NULL, path) == RES_BAD_ARG);
  CHK(rnsl_load(rnsl, NULL) == RES_BAD_ARG);
  CHK(rnsl_load(rnsl, path) == RES_OK);
  CHK(rnsl_get_strings_count(rnsl) == 8);
  CHK(!strcmp(rnsl_get_string(rnsl, 0), "string0"));
  CHK(!strcmp(rnsl_get_string(rnsl, 1), "my/string/1"));
  CHK(!strcmp(rnsl_get_string(rnsl, 2), "my"));
  CHK(!strcmp(rnsl_get_string(rnsl, 3), "my string\t3"));
  CHK(!strcmp(rnsl_get_string(rnsl, 4), "/hello"));
  CHK(!strcmp(rnsl_get_string(rnsl, 5), "world!"));
  CHK(!strcmp(rnsl_get_string(rnsl, 6), "foo"));
  CHK(!strcmp(rnsl_get_string(rnsl, 7), "bar"));

  CHK(setenv("VARIABLE", "my path", 0) == 0);
  CHK(rnsl_load_stream(NULL, fp, path) == RES_BAD_ARG);
  CHK(rnsl_load_stream(rnsl, NULL, path) == RES_BAD_ARG);
  CHK(rnsl_load_stream(rnsl, fp, NULL) == RES_OK);
  CHK(rnsl_get_strings_count(rnsl) == 8);
  CHK(!strcmp(rnsl_get_string(rnsl, 3), "my string\t3"));
  CHK(!strcmp(rnsl_get_string(rnsl, 4), "my path/hello"));
  CHK(!strcmp(rnsl_get_string(rnsl, 5), "world!"));
  rewind(fp);

  CHK(rnsl_load_stream(rnsl, fp, path) == RES_OK);
  CHK(rnsl_get_strings_count(rnsl) == 8);
  CHK(!strcmp(rnsl_get_string(rnsl, 0), "string0"));
  CHK(!strcmp(rnsl_get_string(rnsl, 2), "my"));
  CHK(!strcmp(rnsl_get_string(rnsl, 7), "bar"));

  fclose(fp);
}

static void
test_load_failure(struct rnsl* rnsl)
{
  FILE* fp;

  CHK(fp = tmpfile());

  /* Empty file */
  CHK(rnsl_load_stream(rnsl, fp, NULL) == RES_BAD_ARG);

  /* Invalid #lines */
  fprintf(fp, "2\n");
  rewind(fp);
  CHK(rnsl_load_stream(rnsl, fp, NULL) == RES_BAD_ARG);

  /* Invalid #lines */
  rewind(fp);
  fprintf(fp, "2\n");
  fprintf(fp, "\"string 0\"\n");
  rewind(fp);
  CHK(rnsl_load_stream(rnsl, fp, NULL) == RES_BAD_ARG);

  CHK(fclose(fp) == 0);
}

static void
test_load_file(struct rnsl* rnsl, int argc, char** argv)
{
  int i;
  ASSERT(rnsl && argc && argv);

  FOR_EACH(i, 1, argc) {
    printf("Load %s\n", argv[i]);
    CHK(rnsl_load(rnsl, argv[i]) == RES_OK);
  }
}

int
main(int argc, char** argv)
{
  struct rnsl_create_args args = RNSL_CREATE_ARGS_DEFAULT;
  struct rnsl* rnsl = NULL;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(rnsl_create(&args, &rnsl) == RES_OK);

  if(argc > 1) {
    test_load_file(rnsl, argc, argv);
  } else {
    test_load(rnsl);
    test_load_failure(rnsl);
  }

  CHK(rnsl_ref_put(rnsl) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
