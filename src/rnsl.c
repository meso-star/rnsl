/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* strtok_r and wordexp */

#include "rnsl.h"
#include "rnsl_c.h"
#include "rnsl_log.h"

#include <rsys/cstr.h>
#include <rsys/text_reader.h>

#include <string.h>
#include <wordexp.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
parse_string(struct rnsl* rnsl, struct txtrdr* txtrdr, struct str* str)
{
  wordexp_t wexp;
  char* tk = NULL;
  char* tk_ctx = NULL;
  int wexp_is_allocated = 0;
  res_T res = RES_OK;
  int err = 0;
  ASSERT(rnsl && txtrdr && str);

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(rnsl, "%s: can't read the line `%lu' -- %s\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) {
    const size_t nexpect = darray_str_size_get(&rnsl->strings);
    const size_t nparsed = (size_t)(str - darray_str_cdata_get(&rnsl->strings));
    log_err(rnsl,
      "%s:%lu: missing a string. "
      "Expecting %lu string%swhile %lu %s parsed.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      (unsigned long)nexpect, nexpect == 1 ? " " : "s ",
      (unsigned long)nparsed, nparsed > 1 ? "were" : "was");
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(txtrdr_get_line(txtrdr), "", &tk_ctx);
  ASSERT(tk);

  err = wordexp(tk, &wexp, 0/*flags*/);
  if(err) {
    log_err(rnsl, "%s:%lu: unable to expand string\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }
  wexp_is_allocated = 1;
  ASSERT(wexp.we_wordc != 0);

  if(wexp.we_wordc > 1) {
    log_warn(rnsl,
      "%s:%lu: multiple strings on the same line. "
      "Only the first one will be loaded\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
  }

  res = str_set(str, wexp.we_wordv[0]);
  if(res != RES_OK) {
    log_err(rnsl, "%s:%lu: unable to store the parsed string `%s' -- %s\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      wexp.we_wordv[0], res_to_cstr(res));
    goto error;
  }

exit:
  if(wexp_is_allocated) wordfree(&wexp);
  return res;
error:
  goto exit;
}


static res_T
load_stream(struct rnsl* rnsl, FILE* file, const char* path)
{
  struct txtrdr* txtrdr = NULL;
  char* tk = NULL;
  char* tk_ctx = NULL;
  size_t istr = 0;
  unsigned long nstrs = 0;
  res_T res = RES_OK;
  ASSERT(rnsl && file && path);

  darray_str_clear(&rnsl->strings); /* Clean up */

  res = txtrdr_stream(rnsl->allocator, file, path, '#', &txtrdr);
  if(res != RES_OK) {
    log_err(rnsl, "could not create text reader to parse file `%s' -- %s\n",
      path, res_to_cstr(res));
    goto error;
  }

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(rnsl, "%s: can't read the line %lu --%s\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) {
    log_err(rnsl, "%s: file cannot be empty\n", txtrdr_get_name(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk);

  res = cstr_to_ulong(tk, &nstrs);
  if(res == RES_OK && nstrs == 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(rnsl, "%s:%lu: invalid number of string %lu\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      nstrs);
    goto error;
  }

  res = darray_str_resize(&rnsl->strings, nstrs);
  if(res != RES_OK) {
    log_err(rnsl, "%s: could not allocate the list of %lu strings -- %s\n",
      txtrdr_get_name(txtrdr), nstrs, res_to_cstr(res));
    goto error;
  }

  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(tk) {
    log_warn(rnsl, "%s:%lu: unexpected text `%s'\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

  FOR_EACH(istr, 0, nstrs) {
    struct str* str = darray_str_data_get(&rnsl->strings)+istr;
    res = parse_string(rnsl, txtrdr, str);
    if(res != RES_OK) goto error;
  }

exit:
  if(txtrdr) txtrdr_ref_put(txtrdr);
  return res;
error:
  darray_str_clear(&rnsl->strings);
  goto exit;
}


static INLINE res_T
check_rnsl_create_args(const struct rnsl_create_args* args)
{
  /* Nothing to check. Only return RES_BAD_ARG if args is NULL */
  return args ? RES_OK : RES_BAD_ARG;
}

static void
release_rnsl(ref_T* ref)
{
  struct rnsl* rnsl;
  ASSERT(ref);
  rnsl = CONTAINER_OF(ref, struct rnsl, ref);
  if(rnsl->logger == &rnsl->logger__) logger_release(&rnsl->logger__);
  darray_str_release(&rnsl->strings);
  MEM_RM(rnsl->allocator, rnsl);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
rnsl_create
  (const struct rnsl_create_args* args,
   struct rnsl** out_rnsl)
{
  struct rnsl* rnsl = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_rnsl) { res = RES_BAD_ARG; goto error; }
  res = check_rnsl_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  rnsl = MEM_CALLOC(allocator, 1, sizeof(*rnsl));
  if(!rnsl) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the Rad-Net String List device.\n"
      if(args->logger) {
        logger_print(args->logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&rnsl->ref);
  rnsl->allocator = allocator;
  rnsl->verbose = args->verbose;
  darray_str_init(rnsl->allocator, &rnsl->strings);
  if(args->logger) {
    rnsl->logger = args->logger;
  } else {
    setup_log_default(rnsl);
  }

exit:
  if(out_rnsl) *out_rnsl = rnsl;
  return res;
error:
  if(rnsl) {
    RNSL(ref_put(rnsl));
    rnsl = NULL;
  }
  goto exit;
}

res_T
rnsl_ref_get(struct rnsl* rnsl)
{
  if(!rnsl) return RES_BAD_ARG;
  ref_get(&rnsl->ref);
  return RES_OK;
}

res_T
rnsl_ref_put(struct rnsl* rnsl)
{
  if(!rnsl) return RES_BAD_ARG;
  ref_put(&rnsl->ref, release_rnsl);
  return RES_OK;
}

res_T
rnsl_load(struct rnsl* rnsl, const char* path)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!rnsl || !path) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(rnsl, "%s: error opening file `%s'.\n", FUNC_NAME, path);
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(rnsl, file, path);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
rnsl_load_stream
  (struct rnsl* rnsl,
   FILE* stream,
   const char* stream_name)
{
  if(!rnsl || !stream) return RES_BAD_ARG;
  return load_stream(rnsl, stream, stream_name ? stream_name : "<stream>");
}

size_t
rnsl_get_strings_count(const struct rnsl* rnsl)
{
  ASSERT(rnsl);
  return darray_str_size_get(&rnsl->strings);
}

const char*
rnsl_get_string(const struct rnsl* rnsl, const size_t istring)
{
  ASSERT(rnsl && istring < rnsl_get_strings_count(rnsl));
  return str_cget(darray_str_cdata_get(&rnsl->strings)+istring);
}
