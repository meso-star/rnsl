/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNSL_H
#define RNSL_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(RNSL_SHARED_BUILD) /* Build shared library */
  #define RNSL_API extern EXPORT_SYM
#elif defined(RNSL_STATIC) /* Use/build static library */
  #define RNSL_API extern LOCAL_SYM
#else /* Use shared library */
  #define RNSL_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the rnsl function `Func'
 * returns an error. One should use this macro on suvm function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define RNSL(Func) ASSERT(rnsl_ ## Func == RES_OK)
#else
  #define RNSL(Func) rnsl_ ## Func
#endif

struct rnsl_create_args {
  struct logger* logger; /* May be NULL <=> default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define RNSL_CREATE_ARGS_DEFAULT__ {NULL, NULL, 0}
static const struct rnsl_create_args RNSL_CREATE_ARGS_DEFAULT =
  RNSL_CREATE_ARGS_DEFAULT__;

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

/* Forward declaration of opaque data types */
struct rnsl;

BEGIN_DECLS

/*******************************************************************************
 * API of the Rad-Net file list 
 ******************************************************************************/
RNSL_API res_T
rnsl_create
  (const struct rnsl_create_args* args,
   struct rnsl** rnsl);

RNSL_API res_T
rnsl_ref_get
  (struct rnsl* rnsl);

RNSL_API res_T
rnsl_ref_put
  (struct rnsl* rnsl);

RNSL_API res_T
rnsl_load
  (struct rnsl* rnsl,
   const char* filename);

RNSL_API res_T
rnsl_load_stream
  (struct rnsl* rnsl,
   FILE* stream,
   const char* stream_name);

RNSL_API size_t
rnsl_get_strings_count
  (const struct rnsl* rnsl);

RNSL_API const char*
rnsl_get_string
  (const struct rnsl* rnsl,
   const size_t istring);

END_DECLS

#endif /* RNSL_H */
