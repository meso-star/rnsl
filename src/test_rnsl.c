/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rnsl.h"

#include <rsys/logger.h>

static void
log_stream(const char* msg, void* ctx)
{
  ASSERT(msg);
  (void)msg, (void)ctx;
  printf("%s\n", msg);
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct logger logger;
  struct rnsl_create_args args = RNSL_CREATE_ARGS_DEFAULT;
  struct rnsl* rnsl;
  (void)argc, (void)argv;

  CHK(rnsl_create(NULL, &rnsl) == RES_BAD_ARG);
  CHK(rnsl_create(&args, NULL) == RES_BAD_ARG);
  CHK(rnsl_create(&args, &rnsl) == RES_OK);

  CHK(rnsl_ref_get(NULL) == RES_BAD_ARG);
  CHK(rnsl_ref_get(rnsl) == RES_OK);
  CHK(rnsl_ref_put(NULL) == RES_BAD_ARG);
  CHK(rnsl_ref_put(rnsl) == RES_OK);
  CHK(rnsl_ref_put(rnsl) == RES_OK);

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);
  args.allocator = &allocator;
  args.verbose = 1;
  CHK(rnsl_create(&args, &rnsl) == RES_OK);
  CHK(rnsl_ref_put(rnsl) == RES_OK);

  CHK(logger_init(&allocator, &logger) == RES_OK);
  logger_set_stream(&logger, LOG_OUTPUT, log_stream, NULL);
  logger_set_stream(&logger, LOG_ERROR, log_stream, NULL);
  logger_set_stream(&logger, LOG_WARNING, log_stream, NULL);

  args.logger = &logger;
  args.verbose = 0;
  CHK(rnsl_create(&args, &rnsl) == RES_OK);
  CHK(rnsl_ref_put(rnsl) == RES_OK);
  args.allocator = NULL;
  CHK(rnsl_create(&args, &rnsl) == RES_OK);
  CHK(rnsl_ref_put(rnsl) == RES_OK);

  logger_release(&logger);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

