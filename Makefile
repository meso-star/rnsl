# Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
# Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
# Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
# Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2022, 2023 Observatoire de Paris
# Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
# Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
# Copyright (C) 2022, 2023 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = librnsl.a
LIBNAME_SHARED = librnsl.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC = src/rnsl.c src/rnsl_log.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(RSYS_LIBS)

$(LIBNAME_STATIC): librnsl.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

librnsl.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -DRNSL_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    rnsl.pc.in > rnsl.pc

rnsl-local.pc: rnsl.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    rnsl.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" rnsl.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/rad-net" src/rnsl.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/rnsl" COPYING README.md
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man5" rnsl.5

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/rnsl.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/rnsl/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/rnsl/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/rad-net/rnsl.h"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/rnsl.5"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test librnsl.o rnsl.pc rnsl-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh
	mandoc -Tlint -Wall rnsl.5 || [ $$? -le 1 ]

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_rnsl.c\
 src/test_rnsl_load.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
RNSL_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags rnsl-local.pc)
RNSL_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs rnsl-local.pc)

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

.test: Makefile make.sh
	@$(SHELL) make.sh config_test $(TEST_SRC) > $@

clean_test:
	$(SHELL) make.sh clean_test $(TEST_SRC) file.txt

$(TEST_DEP): config.mk rnsl-local.pc
	@$(CC) $(CFLAGS_EXE) $(RNSL_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk rnsl-local.pc
	$(CC) $(CFLAGS_EXE) $(RNSL_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_rnsl test_rnsl_load: config.mk rnsl-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(RNSL_LIBS) $(RSYS_LIBS)
