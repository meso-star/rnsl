# Rad-Net String List

This C library loads a set of strings listed according to the rnsl file
format. See `rnsl.5` for format specification.

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [mandoc](https://mandoc.bsd.lv)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

###  Version 0.1

- Write the man page directly in mdoc's roff macros, instead of using
  the scdoc markup language as a source for man pages.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

## Copyrights

Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique  
Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace  
Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris  
Copyright (C) 2022, 2023 [|Méso|Star>](https://www.meso-star.com) (contact@meso-star.com)  
Copyright (C) 2022, 2023 Observatoire de Paris  
Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne  
Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin  
Copyright (C) 2022, 2023 Université Paul Sabatier

## License

Rad-Net String List is free software released under the GPL v3+ license:
GNU GPL version 3 or later. You are welcome to redistribute it under
certain conditions; refer to the COPYING file for details.
